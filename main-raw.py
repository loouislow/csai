#####################
##### PYTHON AI #####
#####################
#
# @@script: main-raw.py
# @@description:
# @@author: Loouis Low
# @@copyright: GNU GPL
#

import aiml
import os

kernel = aiml.Kernel()

if os.path.isfile("pattern_brain.brn"):
    kernel.bootstrap(brainFile = "pattern_brain.brn")
else:
    kernel.bootstrap(learnFiles = os.path.abspath("patterns/std-startup.xml"), commands = "load aiml b")
    kernel.saveBrain("pattern_brain.brn")

# kernel now ready for use
while True:
    message = raw_input("Enter your message to the bot: ")
    if message == "quit":
        exit()
    elif message == "save":
        kernel.saveBrain("pattern_brain.brn")
    else:
        bot_response = kernel.respond(message)
        print bot_response
