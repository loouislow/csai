#####################
##### PYTHON AI #####
#####################
#
# @@script: main-raw.py
# @@description:
# @@author: Loouis Low
# @@copyright: GNU GPL
#

from flask import Flask, render_template, request, jsonify
import aiml
import os

app = Flask(__name__)

@app.route("/")
def hello():
    return render_template('interface.html')

@app.route("/ask", methods=['POST'])
def ask():
	message = str(request.form['messageText'])

	kernel = aiml.Kernel()

	if os.path.isfile("pattern_brain.brn"):
	    kernel.bootstrap(brainFile = "pattern_brain.brn")
	else:
	    kernel.bootstrap(learnFiles = os.path.abspath("patterns/std-startup.xml"), commands = "load aiml b")
	    kernel.saveBrain("pattern_brain.brn")

	# kernel now ready for use
	while True:
	    if message == "quit":
	        exit()
	    elif message == "save":
	        kernel.saveBrain("pattern_brain.brn")
	    else:
	        bot_response = kernel.respond(message)
	        # print bot_response
	        return jsonify({'status':'OK','answer':bot_response})

if __name__ == "__main__":
    app.run(debug=True)
