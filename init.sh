#!/bin/bash
#####################
##### PYTHON AI #####
#####################
#
# @@script: install-python-ai
# @@description: Python A.I. Components
# @@author: Loouis Low
# @@copyright: GNU GPL
#

### runas root
function runas_root {
   if [ "$(whoami &2>/dev/null)" != "root" ] && [ "$(id -un &2>/dev/null)" != "root" ]
     then
       echo "[csai] Permission denied."
     exit 1
   fi
}

### install require packages
function check_prerequisites {
    echo "[csai] Installing require packages..." >&2
    sudo apt-get install -y python-pip

    pip install --upgrade pip
    pip install Flask
    pip install aiml

    pip install --upgrade pip
}

### run server
function run_ai_server {
    echo "[csai] starting A.I. server..."
    python main.py
}

### initialize
#runas_root
check_prerequisites
run_ai_server
